<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180417_122138_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'cost' => $this->string(20),
            'date_start' => $this->dateTime(),
            'date_end' => $this->dateTime(),
        ]);

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-project-user_id',
            'project',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
    }
}
