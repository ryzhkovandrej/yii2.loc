<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180417_115906_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'username' => $this->string(20)->notNull(),
            'password' => $this->string(60)->notNull(),
//            'salt' => $this->string(9)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
