<?php
use yii\db\Migration;

/**
 * Class m180417_133234_add_admin_user_to_user_table
 */
class m180417_133234_add_admin_user_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $salt = substr(uniqid(mt_rand(), true), 0, 9);
//        $password = hash('sha1', $salt.'admin');
        $password = Yii::$app->security->generatePasswordHash('admin');
        $this->insert('user', [
            'fio' => 'admin',
            'username' => 'admin',
            'password' => $password,
//            'salt' => $salt,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['login' => 'admin']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180417_133234_add_admin_user_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
